import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
dotenvPath = os.path.join(basedir, '.env')

load_dotenv(dotenvPath)


WTF_CSRF_ENABLED = True
SECRET_KEY = os.environ.get("SECRET_KEY", None)
# UPLOADS_FOLDER = 'uploads/'
# ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

#set the location of the database
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_TRACK_MODIFICATIONS = True


GOOGLE_LOGIN_CLIENT_ID = os.environ.get("GOOGLE_LOGIN_CLIENT_ID", None)
GOOGLE_LOGIN_CLIENT_SECRET = os.environ.get("GOOGLE_LOGIN_CLIENT_SECRET", None)
GOOGLE_DISCOVERY_URL = ("https://accounts.google.com/.well-known/openid-configuration")

URL_SITE = "http://localhost/"
URL_UPLOADS = URL_SITE + 'static/uploads/'