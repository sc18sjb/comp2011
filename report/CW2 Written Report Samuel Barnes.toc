\contentsline {section}{\numberline {1}Introduction}{2}{section.1}% 
\contentsline {subsection}{\numberline {1.1}Purpose of the Web Application}{2}{subsection.1.1}% 
\contentsline {subsection}{\numberline {1.2}Requirements}{3}{subsection.1.2}% 
\contentsline {subsection}{\numberline {1.3}Features Implemented}{3}{subsection.1.3}% 
\contentsline {subsubsection}{\numberline {1.3.1}Advanced Features}{4}{subsubsection.1.3.1}% 
\contentsline {section}{\numberline {2}Accessing the Web Application}{5}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Running the Web Application Locally}{5}{subsection.2.1}% 
\contentsline {section}{\numberline {3}Analysis of the Web Application Architecture}{6}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Presentational}{6}{subsection.3.1}% 
\contentsline {subsubsection}{\numberline {3.1.1}Forms}{8}{subsubsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.2}Business Logic}{9}{subsection.3.2}% 
\contentsline {subsubsection}{\numberline {3.2.1}Views and Web Forms}{9}{subsubsection.3.2.1}% 
\contentsline {subsubsection}{\numberline {3.2.2}Sessions \& Authentication}{13}{subsubsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.3}Data Access}{14}{subsection.3.3}% 
\contentsline {section}{\numberline {4}Potential Security Risks}{16}{section.4}% 
\contentsline {subsection}{\numberline {4.1}CSRF}{17}{subsection.4.1}% 
\contentsline {subsection}{\numberline {4.2}XSS}{17}{subsection.4.2}% 
\contentsline {subsection}{\numberline {4.3}SQL Injection}{17}{subsection.4.3}% 
\contentsline {section}{\numberline {5}Evaluation}{18}{section.5}% 
\contentsline {subsection}{\numberline {5.1}Testing}{18}{subsection.5.1}% 
\contentsline {subsubsection}{\numberline {5.1.1}Test Plan}{18}{subsubsection.5.1.1}% 
\contentsline {subsubsection}{\numberline {5.1.2}Test Results}{20}{subsubsection.5.1.2}% 
\contentsline {subsection}{\numberline {5.2}Meeting the specification}{20}{subsection.5.2}% 
\contentsline {subsubsection}{\numberline {5.2.1}database design}{21}{subsubsection.5.2.1}% 
\contentsline {subsubsection}{\numberline {5.2.2}Sessions and authentication}{22}{subsubsection.5.2.2}% 
\contentsline {subsubsection}{\numberline {5.2.3}Styling}{22}{subsubsection.5.2.3}% 
\contentsline {subsubsection}{\numberline {5.2.4}Unit Testing}{23}{subsubsection.5.2.4}% 
\contentsline {subsubsection}{\numberline {5.2.5}Logging}{23}{subsubsection.5.2.5}% 
\contentsline {subsubsection}{\numberline {5.2.6}Advanced Features}{24}{subsubsection.5.2.6}% 
\contentsline {subsubsection}{\numberline {5.2.7}Deployment}{24}{subsubsection.5.2.7}% 
\contentsline {subsection}{\numberline {5.3}User Experience}{24}{subsection.5.3}% 
\contentsline {subsection}{\numberline {5.4}Reflection}{25}{subsection.5.4}% 
\contentsline {section}{\numberline {6}References}{26}{section.6}% 
