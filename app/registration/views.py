from flask import render_template, url_for, flash, redirect, request, jsonify, session, abort, Blueprint
from flask_login import login_user, logout_user, current_user, login_required
from app import app, db, bcrypt
from .forms import registerUserForm, registerGroupForm
from app.models import User, Company, Address
from app.utils import getSession, saveImage


registration = Blueprint('registration', __name__)


#let the user decide to register as either a company or an individual using the site alont
@registration.route('/register', methods=('GET', 'POST'))
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))

    return render_template('register.html',
                            header = True,
                            title = "register")

#add just a user to the database - represent an individual using the website (not a company)
@registration.route('/register/personal', methods=('GET', 'POST'))
def registerPersonal():
    form = registerUserForm()

    if form.validate_on_submit():
        hashedPwd = bcrypt.generate_password_hash(form.confirmPassword.data).decode('utf-8')

        user = User(first = form.first.data,
                    last = form.last.data,
                    email = form.email.data,
                    pwd = hashedPwd)
        db.session.add(user)
        db.session.commit()

        login_user(user, remember = False)
        session['orders'], session['listings'],session['claimed'] = getSession()

        flash("Successfully Registered!")
        return redirect(url_for('main.home'))

    if form.errors:
        app.logger.warning("Error Registering User: " + str(form.errors))

    return render_template('register/personal.html',
                            header = False,
                            form = form,
                            title = "register | personal")



#add a company to the database, including the user representing that company,
# the details of said company and its address
@registration.route('/register/company', methods=('GET', 'POST'))
def registerCompany():
    form = registerGroupForm()

    if form.validate_on_submit():
        hashedPwd = bcrypt.generate_password_hash(form.about_you.confirmPassword.data).decode('utf-8')

        user = User(first = form.about_you.first.data,
                    last = form.about_you.last.data,
                    email = form.about_you.email.data,
                    pwd = hashedPwd)
        db.session.add(user)
        db.session.commit()

        address = Address(street1 = form.address.street1.data,
                            street2 = form.address.street2.data,
                            city = form.address.city.data,
                            state = form.address.state.data,
                            postal = form.address.postal.data,
                            country = form.address.country.data)
        db.session.add(address)
        db.session.commit()

        pictureFile = None
        if form.company_image.image_data.data != '':
            pictureFile = form.company_image.image_data.data

        # add the address first then add the id of this address to the company
        company = Company(name = form.about_company.cname.data,
                            image = pictureFile,
                            typeof = form.about_company.ctype.data,
                            user_id = user.id,
                            address_id = address.id)

        db.session.add(company)
        db.session.commit()

        login_user(user, remember = False)
        session['orders'], session['listings'],session['claimed'] = getSession()

        return redirect(url_for('accounts.account'))

    if form.errors:
        app.logger.warning("Error Registering Company: " + str(form.errors))

    return render_template('register/company.html',
                            header = False,
                            form = form,
                            title = "register | company")



#check the user details when the company is creating their profile
#the user represents who will log in on behalf of the company
@registration.route('/register/company/response-1', methods=('GET', 'POST'))
def companyResponse1():
    form = registerGroupForm()

    if form.about_you.validate(form):
        return jsonify({'first': form.about_you.first.data,
                        'last': form.about_you.last.data,
                        'email': form.about_you.email.data,
                        'password': form.about_you.confirmPassword.data,
                        'confirmPassword': form.about_you.confirmPassword.data})

    if form.errors:
        app.logger.warning("Error Registering User for Company: " + str(form.errors))

    return jsonify({'error': form.about_you.errors})



#check the company details when the company is creating their profile
@registration.route('/register/company/response-2', methods=('GET', 'POST'))
def companyResponse2():
    form = registerGroupForm()

    if form.about_company.validate(form):
        return jsonify({'cname': form.about_company.cname.data,
                        'ctype': form.about_company.ctype.data})

    if form.errors:
        app.logger.warning("Error Registering Company Details: " + str(form.errors))

    return jsonify({'error': form.about_company.errors})



#check the image to represent the company and upload the image to the server
#return the filepath of the newely uploaded image (if successfully) else the erorr
@registration.route('/register/company/response-3', methods=('GET', 'POST'))
def companyResponse3() :
    form = registerGroupForm()

    if form.company_image.validate(form):
        pictureFile = None
        if (form.company_image.image.data):
            pictureFile = saveImage(form.company_image.image.data, 'company_profile', 50, 50)

        return jsonify({'image': pictureFile})

    if form.errors:
        app.logger.warning("Error Registering Company Image: " + str(form.errors))

    return jsonify({'error': form.company_image.errors})



#check the address details when the company is creating their profile
@registration.route('/register/company/response-4', methods=('GET', 'POST'))
def companyResponse4():
    form = registerGroupForm()

    if form.address.validate(form):
        return jsonify({'street1': form.address.street1.data,
                        'street2': form.address.street2.data,
                        'city': form.address.city.data,
                        'state': form.address.state.data,
                        'postal': form.address.postal.data,
                        'country': form.address.country.data})

    if form.errors:
        app.logger.warning("Error Registering Company Address: " + str(form.errors))

    return jsonify({'error': form.address.errors})
