from app import app, db
from flask import render_template, url_for, flash, redirect, request, jsonify, session, abort, Blueprint
from flask_login import current_user, login_required
from .forms import buyListingForm, createListingForm, unlistForm, relistForm
from app.models import User, Company, Address, Listing, CompanyListing, UserListing, Orders
from app.utils import getSession, saveImage
from datetime import datetime
import json

listings = Blueprint('listings', __name__)


#all the listings that the company / user has created
@listings.route('/account/listings', methods=('GET', 'POST'))
@login_required
def allListings():
    unlist = unlistForm()
    relist = relistForm()
    company = Company.query.filter_by(user_id = current_user.id).first()
    today = datetime.now()


    if company:
        listings = db.session.query(Listing, Address)\
            .join(CompanyListing, CompanyListing.listing_id == Listing.id)\
            .join(Company)\
            .join(Address)\
            .filter(CompanyListing.company_id == company.id)\
            .all()
    else:
        listings = db.session.query(Listing, Address)\
            .join(UserListing, UserListing.listing_id == Listing.id)\
            .join(User)\
            .join(Address)\
            .filter(UserListing.user_id == current_user.id)\
            .all()


    return render_template('account/listings.html',
                                listings = listings,
                                unlist = unlist,
                                relist = relist,
                                today = today,
                                header = True,
                                title = "account | listings")




#show a specific listing. Takes the ID of the listing that is being shown
#if the listingId doesnt exist, redirect to the home page, else show the listing
@listings.route('/listing/<int:listingId>', methods=('GET', 'POST'))
def viewListing(listingId):
    today = datetime.now()

    listing = Listing.query.get_or_404(listingId)

    company = db.session.query(Listing, Company, Address)\
        .join(CompanyListing, CompanyListing.listing_id == Listing.id)\
        .join(Company).join(Address)\
        .filter(Listing.sold.is_(False))\
        .filter(Listing.id == listingId)\
        .first()

    users = db.session.query(Listing, User, Address)\
            .join(UserListing, UserListing.listing_id == Listing.id)\
            .join(User).join(Address)\
            .filter(Listing.sold.is_(False))\
            .filter(Listing.id == listingId)\
            .first()

    #if the listingId doesnt exist
    if not company and not users:
        app.logger.error(f"Trying to access Listing #{listingId}, which doesn't exist")
        return redirect(url_for('main.home'))

    item = company if company else users

    #if the listing has expired (been brought or out of date)
    if item.Listing.sold or item.Listing.time < today:
        return redirect(url_for('main.home')) 

    form = buyListingForm()

    return render_template('listing.html',
                            today = today,
                            item = item,
                            form = form,
                            header = True,
                            title = 'listing')



#buy a specified listing
#only process the listing as brought if the form is valid and the listingId exists, else redirect
#when a listing is brought descrease its quanitity and mark it as sold if quantity <= 0
@listings.route('/listing/buy/<int:listingId>', methods=('GET', 'POST'))
@login_required
def buyListing(listingId):
    form = buyListingForm()

    if form.validate_on_submit():
        item = Listing.query.get_or_404(listingId)
        current = datetime.now()

        order = Orders(user_id = current_user.id, listing_id = item.id, time = current)

        item.quantity = item.quantity - 1 #reduce quantity
        if (item.quantity) <= 0: #check if sold
            item.sold = True

        db.session.add(order)
        db.session.commit()

        session['orders'], session['listings'],session['claimed'] = getSession()
        return redirect(url_for('orders.reciept', orderId = order.id))

    if form.errors:
        app.logger.warning("Error Buying Listing: " + str(form.errors))

    return redirect(url_for('listings.viewListing', listingId = listingId))



#create a listing to be placed on the application
#users and companies create listings on the same page, but using different subforms 
@listings.route('/account/create', methods=('GET', 'POST'))
@login_required
def createListing():
    form = createListingForm()
    company = Company.query.filter_by(user_id = current_user.id).first()

    if form.listing.validate_on_submit(): #if the shared subform is valid (containing the listing details)
        pictureFile = None
        if form.listing_image.image_data.data != '':
            pictureFile = form.listing_image.image_data.data

        if form.listing.price.data.replace(' ', '') == '':
            form.listing.price.data = 0

        #combine the date and time to form a datetime 
        date = datetime.combine(form.listing.date.data, form.listing.time.data)

        # Company create form (without address)
        if company: 
            listing = Listing(name = form.listing.lname.data,
                            price = form.listing.price.data,
                            description = form.listing.ldescription.data,
                            time = date,
                            quantity = form.listing.quantity.data,
                            image = pictureFile)
            db.session.add(listing)
            db.session.commit()

            company_listing = CompanyListing(company_id = company.id,
                                        listing_id = listing.id)
            db.session.add(company_listing)
            db.session.commit()

            #reset the session since listings increased
            session['orders'], session['listings'],session['claimed'] = getSession()
            flash("Listing created successfully")
            return redirect(url_for('main.home'))

        # User create form (with address)
        else:
            if form.address.validate(form): #if the users subform is valid (containing the address of the listing)
                listing = Listing(name = form.listing.lname.data,
                                price = form.listing.price.data,
                                description = form.listing.ldescription.data,
                                time = date,
                                quantity = form.listing.quantity.data,
                                image = pictureFile)

                address = Address(street1 = form.address.street1.data,
                                street2 = form.address.street2.data,
                                city = form.address.city.data,
                                state = form.address.state.data,
                                postal = form.address.postal.data,
                                country = form.address.country.data)

                db.session.add(listing)
                db.session.add(address)
                db.session.commit()

                user_listing = UserListing(user_id = current_user.id,
                                            listing_id = listing.id,
                                            address_id = address.id)
                db.session.add(user_listing)
                db.session.commit()

                #reset the session since listings increased
                session['orders'], session['listings'],session['claimed'] = getSession()
                flash("Listing created successfully")
                return redirect(url_for('main.home'))

    if form.errors:
        app.logger.warning("Error Creating Listing: " + str(form.errors))

    return render_template('account/create.html',
                            form = form,
                            header = True,
                            title = "account | create-listing")



@listings.route('/account/create/response-1', methods=('GET', 'POST'))
@login_required
def createResponse1():
    form = createListingForm()

    if form.listing.validate(form):
        data = {'lname': form.listing.lname.data,
                'ldescription': form.listing.ldescription.data,
                'price': form.listing.price.data,
                'date': form.listing.date.data,
                'time': form.listing.time.data,
                'quantity': form.listing.quantity.data}

        return json.dumps(data, default=str) #add image to the file here so it stays on potential reload

    if form.errors:
        app.logger.warning("Error Creating Listing: " + str(form.errors))

    return jsonify({'error': form.listing.errors})


@listings.route('/account/create/response-2', methods=('GET', 'POST'))
@login_required
def createResponse2():
    form = createListingForm()

    if form.listing_image.validate(form):
        pictureFile = None
        if (form.listing_image.image.data):
            pictureFile = saveImage(form.listing_image.image.data, 'listing_upload', 1000, 500)

        return jsonify({'image': pictureFile})

    if form.errors:
        app.logger.warning("Error Creating Listing: " + str(form.errors))

    return jsonify({'error': form.listing_image.errors})

@listings.route('/account/create/response-3', methods=('GET', 'POST'))
@login_required
def createResponse3():
    form = createListingForm()

    if form.address.validate(form):
        return jsonify({'street1': form.address.street1.data,
                        'street2': form.address.street2.data,
                        'city': form.address.city.data,
                        'state': form.address.state.data,
                        'postal': form.address.postal.data,
                        'country': form.address.country.data})

    if form.errors:
        app.logger.warning("Error Creating Listing: " + str(form.errors))

    return jsonify({'error': form.address.errors})



@listings.route('/account/unlist/<int:listId>', methods=('GET', 'POST'))
@login_required
def unlist(listId):
    form = unlistForm()

    if form.validate_on_submit():
        listing = Listing.query.get_or_404(listId)
        listing.active = False
        db.session.commit()

        flash("Successfully unlisted item!")
        return redirect(url_for('listings.allListings'))

    return redirect(url_for('listings.allListings'))


@listings.route('/account/relist/<int:listId>', methods=('GET', 'POST'))
@login_required
def relist(listId):
    form = relistForm()

    if form.validate_on_submit():
        listing = Listing.query.get_or_404(listId)
        listing.active = True
        db.session.commit()

        flash("Successfully relisted item!")
        return redirect(url_for('listings.allListings'))

    return redirect(url_for('listings.allListings'))


@listings.route('/account/listings/edit/<int:listId>', methods=('GET', 'POST'))
@login_required
def edit_listing(listId):
    today = datetime.now()
    listing = Listing.query.get_or_404(listId)
    form = createListingForm()

    company = db.session.query(Listing, Company, Address)\
        .join(CompanyListing, CompanyListing.listing_id == Listing.id)\
        .join(Company).join(Address)\
        .filter(Listing.id == listId)\
        .first()

    users = db.session.query(Listing, User, Address)\
            .join(UserListing, UserListing.listing_id == Listing.id)\
            .join(User).join(Address)\
            .filter(Listing.id == listId)\
            .first()

    #if the listingId doesnt exist
    if not company and not users:
        app.logger.error(f"Trying to access Listing #{listingId}, which doesn't exist")
        return redirect(url_for('main.home'))

    item = company if company else users

    form.listing.lname.data = item.Listing.name


    return render_template('account/edit-listing.html',
                            today = today,
                            item = item,
                            form = form,
                            header = True,
                            title = "account | listings | edit")