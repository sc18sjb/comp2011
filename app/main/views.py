from app import app, db
from flask import render_template, url_for, flash, redirect, request, session, abort, Blueprint
from app.models import User, Company, Address, Listing, CompanyListing, UserListing
from datetime import datetime
from app.utils import getSession


main = Blueprint('main', __name__, template_folder='templates')



#return if the order was claimed and the time it was claimed at
@main.app_template_filter('claimed')
def _jinja2_filter_claimed(c, time):
    text = ""
    today = datetime.now()

    if c:
        text += "Order claimed"
    else:
        text += "Order cannot be claimed, deadline passed" if time < today else "Order not yet claimed"

    return text



#return the specific time until the listing is delisted,
# so the user doesnt have to caculate it themselves
@main.app_template_filter('strftime')
def _jinja2_filter_datetime(d):
    hours = d.seconds // 3600
    time = ""
    if d.days:
        time += str(d.days) + " Days Left"
    else :
        time += "Available today for " + str(hours) + " hours!"
    return time



#return the datetime split into the day (date) and time (hours, minutes)
@main.app_template_filter('sold')
def _jinja2_filter_sold(sold):
    if sold:
        return "Sold"
    else:
        return "Not Sold"

#return the datetime split into the day (date) and time (hours, minutes)
@main.app_template_filter('strftime2')
def _jinja2_filter_datetime2(d):
    return "By " + d.strftime("%H:%M") + " on " + d.strftime("%b %d %Y")

@main.app_template_filter('brought')
def _jinja2_filter_datetime2(d):
    return "Brought on " + d.strftime("%b %d %Y") + " at " + d.strftime("%H:%M") 


# view all the lists currently available
@main.route('/', methods=('GET', 'POST'))
@main.route('/index', methods=('GET', 'POST'))
def home():
    today = datetime.now()

    #request all the company listings including their address only if the set time is after the current time
    companies = db.session.query(Listing, Company, Address)\
        .join(CompanyListing, CompanyListing.listing_id == Listing.id)\
        .join(Company).join(Address)\
        .filter(Listing.time >= today)\
        .filter(Listing.active.is_(True))\
        .limit(50)\
        .all()

    #request all the user listings including the pickup location of the item only if the set time is after the current time
    users = db.session.query(Listing, User, Address)\
        .join(UserListing, UserListing.listing_id == Listing.id)\
        .join(User).join(Address)\
        .filter(Listing.time >= today)\
        .filter(Listing.active.is_(True))\
        .limit(50)\
        .all()

    listings = companies + users
    listingsToday = []
    listingsSoon = []
    listingsMissed = []
    listingsDefault = []
    listingsLeft = []

    #Sort the listings into categories
    for listing in listings:
        timeLeft = listing.Listing.time - today
        if not listing.Listing.sold:
            if not timeLeft.days: #if the item goes off sale today
                listingsToday.append(listing)
            elif timeLeft.days <= 2:
                listingsSoon.append(listing)
            elif listing.Listing.quantity <= 3:
                listingsLeft.append(listing)
            else: #only items after 5 days and with more than 3 quantities
                listingsDefault.append(listing)
        else: #get when the item was sold and only put recently sold items in
            if len(listingsMissed) <= 5: #only display the most recently sold items (top 5)
                listingsMissed.append(listing)

    count = len(listingsToday) + len(listingsSoon) + len(listingsLeft)\
        + len(listingsDefault) + len(listingsMissed)

    #set a dictionary with the headers and lists for all the different categories of listings to be displayed
    sortedListings = {'Available Today': listingsToday,
                    'Not Long Left': listingsSoon,
                    'Only a Few Left': listingsLeft,
                    'Recently Listed': listingsDefault,
                    'Just Missed It': listingsMissed}


    return render_template('index.html',
                            sortedListings = sortedListings,
                            count = count,
                            today = today,
                            header = True)


