from app import app, db, login_manager #import db variable from __init__.py
from flask_login import UserMixin
from datetime import datetime

@login_manager.user_loader
def loadUser(userId):
    return User.query.get(int(userId))


#the user who is using the web application
class User(db.Model, UserMixin): #mixin the user to return the logged in user 
    id = db.Column(db.Integer, primary_key = True)
    first = db.Column(db.String(32), nullable = False)
    last = db.Column(db.String(64), nullable = False)
    email = db.Column(db.String(128), unique = True, nullable = False)
    pwd = db.Column(db.String(64), nullable = True)
    image = db.Column(db.String(32), nullable = True, default = app.config['URL_UPLOADS'] + 'profile_pics/profile.png')
    external_type = db.Column(db.String(16), nullable = True, default = 'native')
    external_id = db.Column(db.String(64), nullable = True)
    date_created = db.Column(db.DateTime, nullable = False, default = datetime.utcnow())

    listings = db.relationship('UserListing', cascade = "all, delete-orphan", backref = 'user_listings', lazy = True)
    company = db.relationship('Company', cascade = "all, delete-orphan", backref = 'user_company', lazy = True)
    orders = db.relationship('Orders', cascade = "all, delete-orphan", backref = 'user_orders', lazy = True)

    def __repr__(self):
        return f"User('{self.first}', '{self.last}', '{self.email}', '{self.image}', '{self.external_type}', '{self.external_id}', '{self.date_created}')"


#a company that is related to a user who is the representative for the company
class Company(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(64), nullable = False)
    image = db.Column(db.String(32), nullable = True, default = app.config['URL_UPLOADS'] + 'company_profile/company.png')
    typeof = db.Column(db.String(32), nullable = False, default = 'Cafe')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable = False)
    listings = db.relationship('CompanyListing', cascade = "all, delete-orphan", backref = 'company_listings', lazy = True)
    address_id = db.Column(db.Integer, db.ForeignKey('address.id'), nullable = False)

    def __repr__(self):
        return f"Company('{self.name}', '{self.image}', '{self.image}')"


#addresses which can either belong to a company or a users listing 
class Address(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    street1 = db.Column(db.String(256), nullable = False)
    street2 = db.Column(db.String(256), nullable = True)
    city = db.Column(db.String(128), nullable = False)
    state = db.Column(db.String(128), nullable = False)
    postal = db.Column(db.String(16), unique = False, nullable = False)
    country = db.Column(db.String(128), nullable = False)
    company = db.relationship('Company', cascade = "all, delete-orphan", backref = 'address_company', lazy = True)
    listing = db.relationship('UserListing', cascade = "all, delete-orphan", backref = 'address_listing', lazy = True)

    def __repr__(self):
        return f"Address('{self.street1}', '{self.street2}', '{self.city}', '{self.state}', '{self.postal}', '{self.country}')"


#create a connection between the listing and the company who has put the listing up
class CompanyListing(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    company_id = db.Column(db.ForeignKey('company.id'), nullable = False)
    listing_id = db.Column(db.ForeignKey('listing.id'), nullable = False)

    def __repr__(self):
        return f"Company_Listing('{self.company_id}', '{self.listing_id}')"


#create a connection between the listing and the user who has put the listing up
class UserListing(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    user_id = db.Column(db.ForeignKey('user.id'), nullable = False)
    listing_id = db.Column(db.ForeignKey('listing.id'), nullable = False)
    address_id = db.Column(db.Integer, db.ForeignKey('address.id'), nullable = False)

    def __repr__(self):
        return f"User_Listing('{self.user_id}', '{self.listing_id}')"


#to represent an item put up for sale
class Listing(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(256), nullable = False)
    price = db.Column(db.Float, nullable = False)
    description = db.Column(db.Text, nullable = False)
    time = db.Column(db.DateTime, nullable = False)
    image = db.Column(db.String(20), nullable = True, default = app.config['URL_UPLOADS'] + 'listing_upload/food-default.jpg')
    sold = db.Column(db.Boolean, unique = False, default = False)
    active = db.Column(db.Boolean, unique = False, default = True)
    quantity = db.Column(db.Integer, nullable = False, default = 1)
    company = db.relationship('CompanyListing', cascade = "all, delete-orphan", backref = 'listing_company', lazy = True)
    user = db.relationship('UserListing', cascade = "all, delete-orphan", backref = 'listing_user', lazy = True)
    orders = db.relationship('Orders', cascade = "all, delete-orphan", backref = 'listing_orders', lazy = True)
    
    def __repr__(self):
        return f"Listing('{self.name}', '{self.price}', '{self.description}', '{self.time}', '{self.image}', '{self.sold}', '{self.quantity}')"


#Create a connection between the user who has brought the item, and the item they have brought
class Orders(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    user_id = db.Column(db.ForeignKey('user.id'), nullable = False)
    listing_id = db.Column(db.ForeignKey('listing.id'), nullable = False)
    claimed = db.Column(db.Boolean, unique = False, default = False)
    time = db.Column(db.DateTime, nullable = False)

    def __repr__(self):
        return f"Order('{self.user_id}', '{self.listing_id}')"