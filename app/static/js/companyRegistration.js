var pageCount = 0;

$('#form-1').submit(function(e) {
    var errorIds = ['first-error', 'last-error', 'email-error', 'password-error', 'confirmPassword-error'];
    $.each(errorIds, function(key, value) {
        $('#' + value).text("");
        $('#' + value).addClass('d-none')
    });

    var csrf_token = $('#about_you-csrf_token').attr('value');

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        }
    });

    $.ajax({
        url: '/register/company/response-1',
        type: 'POST',
        data: $('#form-1').serialize(),
        success: function(data) {
            if (data.error) {
                $.each(data.error, function(key, value) {
                    $('#' + key + '-error').text(value);
                    $('#' + key + '-error').removeClass('d-none');
                });
            } else {
                pageCount++;
                changePage(pageCount, "next", 3);
                $('#btn-prev').removeClass('d-none');
            }
        },

        error: function(error) {
            alert(error);
        }
    });
    e.preventDefault();
});


$('#form-2').submit(function(e) {
    var errorIds = ['name-error', 'type-error'];
    $.each(errorIds, function(key, value) {
        $('#' + value).text("");
        $('#' + value).addClass('d-none')
    });

    var csrf_token = $('#about_compny-csrf_token').attr('value');

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        }
    });

    $.ajax({
        url: '/register/company/response-2',
        type: 'POST',
        data: $('#form-2').serialize(),
        success: function(data) {
            if (data.error) {
                $.each(data.error, function(key, value) {
                    $('#' + key + '-error').text(value);
                    $('#' + key + '-error').removeClass('d-none');
                });
            } else {
                pageCount++;
                changePage(pageCount, "next", 3);
            }
        },
        error: function(error) {
            alert(error);
        }
    });

    e.preventDefault();
});


$('#image-upload').on('change', function(e) {
    var formData = new FormData($('#form-3')[0]);

    // remove the existing error
    $('#image-error').text("");
    $('#image-error').addClass('d-none')

    var csrf_token = $('#company_image.csrf_token').attr('value');

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        }
    });

    $.ajax({
        url: '/register/company/response-3',
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(data) {
            if (data.error){
                $.each(data.error, function(key, value) {
                    $('#' + key + '-error').text(value);
                    $('#' + key + '-error').removeClass('d-none');
                });
            } else {
                $('#company-image').attr('src', data.image);
                $('#company_image-image_data').attr('value', data.image);
            }
        },
        error: function(error) {
            alert(error);
        }
    });
});


$('#form-3').submit(function(e) {
    e.preventDefault();
    pageCount++;
    changePage(pageCount, "next", 3);
});


$('#form-4').submit(function(e) {
    var errorIds = ['street1-error', 'street2-error', 'city-error', 'state-error', 'postal-error', 'country-error'];
    $.each(errorIds, function(key, value) {
        $('#' + value).text("");
        $('#' + value).addClass('d-none')
    });

    var csrf_token = $('#address-csrf_token').attr('value');

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        }
    });


    $.ajax({
        url: '/register/company/response-4',
        type: 'POST',
        data: $('#form-4').serialize(),
        success: function(data) {
            if (data.error) {
                $.each(data.error, function(key, value) {
                    $('#' + key + '-error').text(value);
                    $('#' + key + '-error').removeClass('d-none');
                });
            } else {
                combineForms();
            }
        },
        error: function(data) {
            alert(error);
        }
    });

    e.preventDefault();
});

$('#form-5').submit(function(e) {
    $('#form-4').submit();
    e.preventDefault();
});