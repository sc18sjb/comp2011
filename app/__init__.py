from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_session import Session
from logging.config import dictConfig
from socket import gethostname
from oauthlib.oauth2 import WebApplicationClient
import logging
import sys
import os


DICT_CONFIG = {
    'version': 1,
    'formatters': {'default': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
        }, 'warning': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s at %(funcName)s: %(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'INFO',
            'stream': 'ext://flask.logging.wsgi_errors_stream'
        },
        'file': {
            'class': 'logging.FileHandler',
            'level': 'WARNING',
            'formatter': 'warning',
            'filename': 'logs/error-logs.log'
        },
        'info': {
            'class': 'logging.FileHandler',
            'level': 'INFO',
            'formatter': 'default',
            'filename': 'logs/info-logs.log'
        }
    }, 'root': {
        'handlers': ('console', 'info', 'file'), 'level': 'INFO'
    }
}

if 'green-liveweb11' not in gethostname(): #if the application is deployed online
    dictConfig(DICT_CONFIG)
else: #if running locally 
    logging.basicConfig(stream=sys.stderr, level=logging.CRITICAL)


app = Flask(__name__) #app global variable
app.config.from_object('config') #configuration file for the application
db = SQLAlchemy(app) #database
bcrypt = Bcrypt(app)
migrate = Migrate(app, db) #datase migrations
login_manager = LoginManager(app)
login_manager.login_view = 'users.login' #function route of the login view - where the user will be redirected if not logged in
login_manager.login_message_category = 'info' #the classes for the flashed message when the user is not logged in

client = WebApplicationClient(app.config['GOOGLE_LOGIN_CLIENT_ID'])

os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

#have to be at bottom since theyre importing the app variable, which is created above
from .users.views import users
from .main.views import main
from .listings.views import listings
from .registration.views import registration
from .accounts.views import accounts
from .orders.views import orders

app.register_blueprint(main)
app.register_blueprint(users)
app.register_blueprint(listings)
app.register_blueprint(registration)
app.register_blueprint(accounts)
app.register_blueprint(orders)
