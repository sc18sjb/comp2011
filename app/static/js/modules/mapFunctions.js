
function formattedAddress(address) {
    return;
}

function toggleBounce() {
	if (marker.getAnimation() !== null) {
		marker.setAnimation(null);
	} else {
		marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}

function initMap() {
    geocoder = new google.maps.Geocoder(); 

    var address = $('#address').text();

    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status !== 'OK') {
            window.alert('Geocoder failed due to: ' + status);
            return;
        }

        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();

            initialize(latitude, longitude);
        } 
    }); 
}

function initialize(latitude, longitude) {
    var markers = [];//some array

    var map = new google.maps.Map(document.getElementById('googleMap'), {
        zoom: 3,
        center: {lat: 53.805471, lng: -1.553438}
    });

    marker = new google.maps.Marker({
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP,
        position: {lat: latitude, lng: longitude}
    });
    marker.addListener('click', toggleBounce);
    markers.push(marker)
  
    var infowindow = new google.maps.InfoWindow({
        content:"Collection Location"
    });
      
    infowindow.open(map,marker);

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			  
			marker = new google.maps.Marker({
				map: map,
				draggable: false,
				animation: google.maps.Animation.DROP,
				position: pos
            });
            markers.push(marker)
            
			var infowindow = new google.maps.InfoWindow({
				content:"Your Location"
			});
			
            infowindow.open(map,marker);
            

            var bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < markers.length; i++) {
                bounds.extend(markers[i].getPosition());
            }

            map.fitBounds(bounds);

			// map.setCenter(pos);
		}, function() {
			handleLocationError(true, infoWindow, map.getCenter());
		});
	} else {
		// Browser doesn't support Geolocation
		handleLocationError(false, infoWindow, map.getCenter());
	}
}