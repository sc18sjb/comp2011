import os
import secrets
from PIL import Image
from datetime import datetime, date
from app import app, db #so routes can use @app variable, import app and db variables from __init__.py
from .models import User, Company, Address, Listing, CompanyListing, UserListing, Orders
from flask_login import login_user, logout_user, current_user, login_required
import json
import requests

def getGoogleProviderCfg():
    return requests.get(app.config['GOOGLE_DISCOVERY_URL']).json()


#return all the session variables used within the application 
def getSession():
    company = Company.query.filter_by(user_id = current_user.id).first()
    orders = Orders.query.filter_by(user_id = current_user.id).count()

    today = datetime.now()
    claimed = db.session.query(Orders, Listing).join(Listing).filter(Orders.user_id == current_user.id)\
        .filter(Orders.claimed.is_(False)).filter(Listing.time >= today).count()

    if company: #if a company, set the listings to that companies listings
        listings = db.session.query(Listing)\
            .join(CompanyListing, CompanyListing.listing_id == Listing.id)\
            .filter(CompanyListing.company_id == company.id)\
            .count()
    else: #else set it to the users listings
        listings = db.session.query(Listing)\
            .join(UserListing, UserListing.listing_id == Listing.id)\
            .filter(UserListing.user_id == current_user.id)\
            .count()


    return orders, listings, claimed


#format and save the image to the correct filepath within the file directory
#returns the name of the new file saved to the directory
def saveImage(formImage, filepath, width, height):
    randomHex = secrets.token_hex(8) #generate a random key for the image (so each name is different)
    _, fExt = os.path.splitext(formImage.filename) #filename from the form data
    pictureFilename = randomHex + fExt 
    picturePath = os.path.join(app.root_path, 'static/uploads/', (filepath + '/'), pictureFilename)

    outputSize = (width, height) #resize the image (only if it is bigger than the specified width, height)
    i = Image.open(formImage)
    i.thumbnail(outputSize, Image.ANTIALIAS)
    i.save(picturePath)

    return app.config['URL_UPLOADS'] + filepath + '/' + pictureFilename