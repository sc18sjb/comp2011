from flask import render_template, url_for, flash, redirect, request, jsonify, session, abort, Blueprint
from app import app, db, bcrypt 
from .forms import updateDetailsForm, updatePasswordForm, deleteAccountForm
from flask_login import login_user, logout_user, current_user, login_required
from app.models import User
from app.utils import saveImage


accounts = Blueprint('accounts', __name__)


#the users specific account page
@accounts.route('/account', methods=('GET', 'POST'))
@login_required
def account():
    return render_template('account/account.html',
                            header = True,
                            title = "account")



@accounts.route('/account/security', methods=('GET', 'POST'))
@login_required
def security():
    form_details = updateDetailsForm()
    form_password = updatePasswordForm()
    form_delete = deleteAccountForm()

    form_details.first.data = current_user.first
    form_details.last.data = current_user.last
    form_details.email.data = current_user.email

    return render_template('account/security.html',
                            form_details = form_details,
                            form_password = form_password,
                            form_delete = form_delete,
                            header = True,
                            title = "account | security")



@accounts.route('/account/security/update_user', methods=('GET', 'POST'))
@login_required
def update_user():
    form_password = updatePasswordForm()
    form_delete = deleteAccountForm()
    form = updateDetailsForm()

    if form.validate_on_submit():
        pictureFile = current_user.image
        if form.image.data:
            pictureFile = saveImage(form.image.data, 'profile_pics', 50, 50)

        user = User.query.get_or_404(current_user.id)
        user.first = form.first.data
        user.last = form.last.data
        user.email = form.email.data
        user.image = pictureFile
        db.session.commit()

        flash("Successfully updated details!")
        return redirect(url_for('accounts.security'))

    return render_template('account/security.html',
                            form_details = form,
                            form_password = form_password,
                            form_delete = form_delete,
                            header = True,
                            title = "account | security")



@accounts.route('/account/security/update_password', methods=('GET', 'POST'))
@login_required
def update_password():
    form = updatePasswordForm()
    form_details = updateDetailsForm()
    form_delete = deleteAccountForm()

    form_details.first.data = current_user.first
    form_details.last.data = current_user.last
    form_details.email.data = current_user.email

    if form.validate_on_submit():
        hashedPwd = bcrypt.generate_password_hash(form.confirmPassword.data).decode('utf-8')
        user = User.query.get_or_404(current_user.id)
        user.pwd = hashedPwd
        db.session.commit()

        flash("Successfully Updated Password!")
        return redirect(url_for('accounts.security'))

    if form.errors:
        app.logger.warning(form.errors)

    return render_template('account/security.html',
                            form_details = form_details,
                            form_password = form,
                            form_delete = form_delete,
                            header = True,
                            title = "account | security")


@accounts.route('/account/security/delete_account', methods=('GET', 'POST'))
@login_required
def delete_account():
    form_details = updateDetailsForm()
    form_password = updatePasswordForm()
    form = deleteAccountForm()

    form_details.first.data = current_user.first
    form_details.last.data = current_user.last
    form_details.email.data = current_user.email


    if form.validate_on_submit():
        #delete the users account

        db.session.delete(current_user)
        db.session.commit()
        return redirect(url_for('main.home'))

    if form.errors:
        app.logger.warning(form.errors)

    return render_template('account/security.html',
                            form_details = form_details,
                            form_password = form_password,
                            form_delete = form,
                            header = True,
                            title = "account | security")