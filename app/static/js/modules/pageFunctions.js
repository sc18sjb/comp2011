function getPageLength(self, defaultLength) {
    if (!defaultLength) defaultLength = 3;
    var attr = self.attr('page-length');
    var length = defaultLength;
    if (typeof attr !== typeof undefined && attr !== false) {
        length = attr;
    }
    return length;
}


function combineForms() {
    var $newForm = $("<form></form>").attr({method: "POST", action: ""});

    $(":input:not(:submit, :button)").each(function() {  
        $newForm.append($("<input type=\"hidden\" />")  
            .attr('name', this.name)   
            .val($(this).val())        
        );
    });
    $newForm.appendTo(document.body).submit();
}


function changePage(count, direction, size) {
    if (count == 0)
        $('#btn-prev').addClass('d-none');

    percent = (count * 100) / size;
    $('.progress-bar').css("width", percent + '%');

    console.log(percent, size);

    if (direction == "next") {
        $('#slide-' + count).addClass('d-none');
        $('#slide-' + (count + 1)).removeClass('d-none');
    } else {
        $('#slide-' + (count + 2)).addClass('d-none');
        $('#slide-' + (count + 1)).removeClass('d-none');
    }
}