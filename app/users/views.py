from flask import render_template, url_for, flash, redirect, request, session, abort, Blueprint
from app import app, db, bcrypt, client #so routes can use @app variable, import app and db variables from __init__.py
from .forms import loginForm
from app.models import User
from flask_login import login_user, logout_user, current_user, login_required
from app.utils import getSession, getGoogleProviderCfg
import requests
import json

users = Blueprint('users', __name__)

#log in the user and redirect them to the accounts page (if the requested to go there),
# else redirect to the home page
@users.route('/login', methods=('GET', 'POST'))
def login():
    if current_user.is_authenticated:
        return redirect(url_for('accounts.account'))

    form = loginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()

        if user: #we dont need to validate the users password, since this is done within the form validation
            login_user(user, remember = form.remember.data)

            session['orders'], session['listings'],session['claimed'] = getSession()

            nextPage = request.args.get('next') #get returns none if the next key doesnt exists (so it better than ['next'])
            flash("Successfully Logged In!")
            return redirect(nextPage) if nextPage else redirect(url_for('accounts.account'))

    if form.errors:
        app.logger.warning("Error Logging User In: " + str(form.errors))

    return render_template('login.html',
                            form = form,
                            header = True,
                            title = "login")


@users.route('/login/gsuite', methods=('GET', 'POST'))
def gsuite():
    google_provider_cfg = getGoogleProviderCfg()
    authorization_endpoint = google_provider_cfg["authorization_endpoint"]

    request_uri = client.prepare_request_uri(
        authorization_endpoint,
        redirect_uri=request.base_url + "/callback",
        scope=["openid", "email", "profile"],
    )
    return redirect(request_uri)


@users.route('/login/gsuite/callback', methods=('GET', 'POST'))
def callback():
    code = request.args.get("code")
  
    # Find out what URL to hit to get tokens that allow you to ask for
    # things on behalf of a user
    google_provider_cfg = getGoogleProviderCfg()
    token_endpoint = google_provider_cfg["token_endpoint"]

    # Prepare and send a request to get tokens! Yay tokens!
    token_url, headers, body = client.prepare_token_request(
        token_endpoint,
        authorization_response=request.url,
        redirect_url=request.base_url,
        code=code
    )
    token_response = requests.post(
        token_url,
        headers=headers,
        data=body,
        auth=(app.config['GOOGLE_LOGIN_CLIENT_ID'], app.config['GOOGLE_LOGIN_CLIENT_SECRET']),
    )

    # Parse the tokens!
    client.parse_request_body_response(json.dumps(token_response.json()))


    # Now that you have tokens (yay) let's find and hit the URL
    # from Google that gives you the user's profile information,
    # including their Google profile image and email
    userinfo_endpoint = google_provider_cfg["userinfo_endpoint"]
    uri, headers, body = client.add_token(userinfo_endpoint)
    userinfo_response = requests.get(uri, headers=headers, data=body)


    if userinfo_response.json().get("email_verified"):
        userId = userinfo_response.json()["sub"]
        userEmail = userinfo_response.json()["email"]
        userImage = userinfo_response.json()["picture"]
        userFirstName = userinfo_response.json()["given_name"]
        userLastName = userinfo_response.json()['family_name']

        user = User.query.filter_by(external_id=userId).first()

        if user:
            login_user(user, remember = False)
            session['orders'], session['listings'],session['claimed'] = getSession()

            nextPage = request.args.get('next') #get returns none if the next key doesnt exists (so it better than ['next'])
            
            flash("Successfully Logged In!")
            return redirect(nextPage) if nextPage else redirect(url_for('accounts.account'))

        # no account exists with the google id
        else:
            user = User.query.filter_by(email = userEmail).first()

            # if an account already exists with the same email, we can link the google account
            if user:
                user.external_id = userId
                db.session.commit()

                flash("Successfully Logged In!")
            else:
                user = User(first = userFirstName,
                            last = userLastName,
                            email = userEmail,
                            image = userImage,
                            external_type = 'google',
                            external_id = userId)
                db.session.add(user)
                db.session.commit()

                flash("Successfully Registered!")


            login_user(user, remember = False)
            session['orders'], session['listings'],session['claimed'] = getSession()

            return redirect(url_for('accounts.account'))

    else:
        app.logger.warning("Error Registering User: User email not available or not verified by Google")

        return "User email not available or not verified by Google.", 400


#end the user session and log the user out
@users.route('/logout')
def logout():
    logout_user()
    session.pop('claimed')
    flash("Successfully logged out!")
    return redirect(url_for('main.home'))