from flask import render_template, url_for, flash, redirect, request, jsonify, session, abort, Blueprint
from app import app, db
from .forms import claimListingForm
from app.models import User, Company, Address, Listing, CompanyListing, UserListing, Orders
from flask_login import login_user, logout_user, current_user, login_required
from datetime import datetime, date
from app.utils import getSession


orders = Blueprint('orders', __name__)


#all the orders from the company / user
@orders.route('/account/orders', methods=('GET', 'POST'))
@login_required
def viewOrders():
    company = db.session.query(Orders, Listing, Company, Address)\
        .join(Listing)\
        .join(CompanyListing)\
        .join(Company)\
        .join(Address)\
        .filter(Orders.user_id == current_user.id)\
        .all()

    user = db.session.query(Orders, Listing, User, Address)\
        .join(Listing)\
        .join(UserListing)\
        .join(User)\
        .join(Address)\
        .filter(Orders.user_id == current_user.id)\
        .all()

    orders = company + user #orders is orders from both companies and users
    deletedOrders = []
    if len(orders) < session.get('orders'):
        deletedOrders = db.session.query(Orders, Listing).join(Listing).filter(Orders.user_id == current_user.id).filter().all()

        for order in orders:
            for deleted in deletedOrders:
                if deleted.Orders in order:
                    deletedOrders.remove(deleted)
                    
    return render_template('account/orders.html',
                            deletedOrders = deletedOrders,
                            orders = orders,
                            header = True,
                            title = "account | orders")




#mark an order as claimed and remove it from the list of orders yet to claimed (mark as claimed)
#if the user tries to access the claimed listing page when no orders are marked to be claimed, 
# redict back to the home page
@orders.route('/claimed', methods=('GET', 'POST'))
@login_required
def claimOrder():
    if session.get('claimed') <= 0: #if there are no orders to claim 
        app.logger.error(f"User #{current_user.id} trying to access claimed orders, where none exist")
        return redirect(url_for('main.home'))

    form = claimListingForm()
    today = datetime.now()

    #reset the session variables since the # of claimed orders has changed
    session['orders'], session['listings'],session['claimed'] = getSession()

    orders = db.session.query(Orders, Listing)\
        .join(Listing)\
        .filter(Orders.user_id == current_user.id)\
        .filter(Orders.claimed.is_(False))\
        .filter(Listing.time >= today)\
        .all()

    return render_template('claimed.html',
                            orders = orders,
                            form = form,
                            header = True,
                            title = 'claimed')



@orders.route('/claimed/order/<int:orderId>', methods=('GET', 'POST'))
@login_required
def claimOrderClaimed(orderId):
    form = claimListingForm()

    if form.validate_on_submit():
        order = Orders.query.get_or_404(orderId)
        order.claimed = True
        db.session.commit()

        session['orders'], session['listings'], session['claimed'] = getSession()

        flash("Order claimed successfully!")
        return redirect(url_for('main.home'))

    if form.errors:
        app.logger.warning("Error Claiming Order: " + str(form.errors))

    return redirect(url_for('orders.claimOrder'))



#show the reciept of a listing specified
#if the listingId doesnt exist, redirect, else show the reciept of that listing
@orders.route('/account/reciept/<int:orderId>', methods=('GET', 'POST'))
@login_required
def reciept(orderId):
    order = Orders.query.get_or_404(orderId) #throw a 404 error if the listing doesnt exist 
    if order:
        if order.user_id != current_user.id: #if someone else is trying to access the reciept
            app.logger.error(f"User #{current_user.id} trying to access reciept #{orderId} which is not theirs")
            return redirect(url_for('main.home'))

        company = db.session.query(Listing, Company, Address)\
            .join(CompanyListing, CompanyListing.listing_id == Listing.id)\
            .join(Company)\
            .join(Address)\
            .filter(Listing.id == order.listing_id)\
            .first()

        users = db.session.query(Listing, User, Address)\
            .join(UserListing, UserListing.listing_id == Listing.id)\
            .join(User)\
            .join(Address)\
            .filter(Listing.id == order.listing_id)\
            .first()

        if company or users:
            item = company if company else users
        else:
            abort(410)

    return render_template('account/reciept.html',
                                header = True,
                                order = order,
                                item = item,
                                title = 'account | reciept#' + str(orderId))