from app import bcrypt
from flask_wtf import FlaskForm  
from flask_wtf.file import FileField, FileAllowed
from wtforms import IntegerField, StringField, SubmitField, PasswordField, BooleanField, SelectField, HiddenField, FormField, FloatField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, EqualTo, Optional, ValidationError, InputRequired, NumberRange
from flask_login import current_user
from app.models import User



class deleteAccountForm(FlaskForm):
    password = PasswordField('Re-enter your password', validators=[DataRequired()],
                                                                        render_kw={"placeholder": "Enter Password"})
    submit = SubmitField('Delete Account')

    def validate_password(self, password):
        if not bcrypt.check_password_hash(current_user.pwd, password.data):
            raise ValidationError('password is incorrect')




class updateDetailsForm(FlaskForm):
    first = StringField('Firstname', validators=[DataRequired(), 
                                                    Length(min=1, max=32)],
                                                    render_kw={"placeholder": "Enter Firstname"})
    last = StringField('Surname', validators=[DataRequired(),
                                                Length(min=1, max=64)],
                                                render_kw={"placeholder": "Enter Surname"})  
    email = StringField('Email', validators=[DataRequired(), 
                                                Email(),
                                                Length(min=1, max=128)],
                                                render_kw={"placeholder": "Enter Email"})
    image = FileField('Profile Picture (optional)', validators=[FileAllowed(['png', 'jpg', 'jpeg'])])
    submit = SubmitField('Update') 

    def validate_email(self, email):
        if email.data != current_user.email:
            user = User.query.filter_by(email = email.data).first()

            if user:
                raise ValidationError('email already taken')




class updatePasswordForm(FlaskForm):
    oldPassword = PasswordField('Current Password', validators=[DataRequired()], 
                                                        render_kw={"placeholder": "Enter Password"})
    password = PasswordField('New Password', validators=[DataRequired()], 
                                                        render_kw={"placeholder": "Enter Password"})
    confirmPassword = PasswordField('Confirm New Password', validators=[DataRequired(), 
                                                                        EqualTo('password')],
                                                                        render_kw={"placeholder": "Confirm Password"})
    submit = SubmitField('Update')


    def validate_oldPassword(self, oldPassword):
        if not bcrypt.check_password_hash(current_user.pwd, oldPassword.data):
            raise ValidationError('password is incorrect')