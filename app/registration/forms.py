from flask_wtf import FlaskForm  
from flask_wtf.file import FileField, FileAllowed
from wtforms import IntegerField, StringField, SubmitField, PasswordField, BooleanField, SelectField, HiddenField, FormField, FloatField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, EqualTo, Optional, ValidationError, InputRequired, NumberRange
from app.models import User
import pycountry


class CountrySelectField(SelectField):
    def __init__(self, *args, **kwargs):
        super(CountrySelectField, self).__init__(*args, **kwargs)
        self.choices = [(country.alpha_2, country.name) for country in pycountry.countries]


class registerUserForm(FlaskForm):
    first = StringField('Firstname', validators=[DataRequired(), 
                                                    Length(min=1, max=32)],
                                                    render_kw={"placeholder": "Enter Firstname"})
    last = StringField('Surname', validators=[DataRequired(),
                                                Length(min=1, max=64)],
                                                render_kw={"placeholder": "Enter Surname"})  
    email = StringField('Email', validators=[DataRequired(), 
                                                Email(),
                                                Length(min=1, max=128)],
                                                render_kw={"placeholder": "Enter Email"})
    password = PasswordField('Password', validators=[DataRequired(), 
                                                        Length(min=6, max=20, message='Password must be more than 6 characters')],
                                                        render_kw={"placeholder": "Enter Password"})
    confirmPassword = PasswordField('Confirm Password', validators=[DataRequired(), 
                                                                        Length(min=6, max=20),
                                                                        EqualTo('password')],
                                                                        render_kw={"placeholder": "Confirm Password"})
    
    submit = SubmitField('Register')


    def validate_email(self, email):
        user = User.query.filter_by(email = email.data).first()

        if user:
            raise ValidationError('email already taken')

    def validate_password(self, password):
        if not any(x.isupper() for x in password.data):
            raise ValidationError('password must contain at least 1 uppercase character')

    
class registerCompanyForm(FlaskForm):
    cname = StringField('Company Name', validators=[DataRequired(),
                                            Length(min=1, max=64)],
                                            render_kw={"placeholder": "Enter Company Name"})
    ctype = SelectField('Are you a?', choices=[('cafe', 'Cafe'), ('restraunt', 'Restraunt'), ('takeaway', 'Takeaway'), ('bar', 'Bar'), 
                                                ('shop', 'Shop'), ('other', 'Other')])
    submit = SubmitField('submit')

class registerCompanyImageForm(FlaskForm):
    image = FileField('Image (optional)', validators=[FileAllowed(['png', 'jpg', 'jpeg'])])
    image_data = HiddenField('image', default=None)
    submit = SubmitField('submit')


class registerAddressForm(FlaskForm):
    street1 = StringField('Address Line 1', validators=[DataRequired(),
                                                        Length(min=1, max=256)],
                                                        render_kw={"placeholder": "Enter Address Line 1"}) 
    street2 = StringField('Address Line 2', validators=[Length(max=256)],
                                                        render_kw={"placeholder": "Enter Address Line 2"})
    city = StringField('City', validators=[DataRequired(),
                                            Length(min=1, max=128)],
                                            render_kw={"placeholder": "Enter City"})
    state = StringField('State / County', validators=[DataRequired(),
                                                        Length(min=1, max=128)],
                                                        render_kw={"placeholder": "Enter State / County"})
    postal = StringField('Postal / Zip Code', validators=[DataRequired(),
                                                            Length(min=1, max=16)],
                                                            render_kw={"placeholder": "Enter Postal / Zip Code"})
    country = CountrySelectField('Country')
    submit = SubmitField('submit')

class registerGroupForm(FlaskForm):
    about_you = FormField(registerUserForm)
    about_company = FormField(registerCompanyForm)
    company_image = FormField(registerCompanyImageForm)
    address = FormField(registerAddressForm)
    submit = SubmitField('submit')
