import os
import unittest
from app import app, db, bcrypt
from app.models import User, Company, Listing, UserListing, CompanyListing, Address, Orders
from flask_testing import TestCase
from flask_login import current_user, logout_user
from flask import Flask, request, jsonify
import collections
from datetime import datetime

TEST_DB = 'test.db'


#### --- test data --- ####
TEST_USER = {
    'first': 'Samuel',
    'last': 'Barnes',
    'email': 'barnes.samb@gmail.com',
    'password': 'python',
    'confirmPassword': 'python'
}

TEST_COMPANY = {
    'cname': 'Sams company',
    'ctype': 'cafe'
}

TEST_COMPANY_IMAGE = {
    'image': '',
    'image_data': ''
}

TEST_ADDRESS = {
    'street1': 'Example',
    'street2': 'Street',
    'city': 'Leeds',
    'state': 'Yorkshire',
    'postal': 'LS1 1NT',
    'country': 'United Kingdom'
}

TEST_LISTING = {
    'lname': 'Example Listing',
    'price': '10.05',
    'ldescription': 'This is an example user listing for our unit tests',
    'time': '2099-12-14T10:52',
    'quantity': '10',
    'image': ''
}


def flatten(d, parent_key='', sep='-'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


class TestClass(TestCase):
    def create_app(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = True
        basedir = os.path.abspath(os.path.dirname(__file__))
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, TEST_DB)
        return app


    def setUp(self):   
        db.create_all()

        if current_user.is_authenticated:
            logout_user()

        hashedPwd = bcrypt.generate_password_hash('python').decode('utf-8')
        db.session.add(
            User(first = 'Example', last = 'User', email = 'example@user.com', pwd = hashedPwd))
        db.session.commit()


    def tearDown(self):
        db.session.remove()
        db.drop_all()

    ### ---- TESTS ---- ####

    ### ---- Test each of the pages load correctly with no errors ---- ###
    def test_home_page_loads(self):
        with self.client:
            response = self.client.get('/', follow_redirects = True)
            self.assertEqual(response.status_code, 200)

    def test_registration_page_loads(self):
        with self.client:
            response = self.client.get('/register', follow_redirects = True)
            self.assertEqual(response.status_code, 200)

    def test_registration_user_page_loads(self):
        with self.client:
            response = self.client.get('/register/personal', follow_redirects = True)
            self.assertEqual(response.status_code, 200)

    def test_registration_company_page_loads(self):
        with self.client:
            response = self.client.get('/register/company', follow_redirects = True)
            self.assertEqual(response.status_code, 200)

    def test_account_page_loads(self):
        with self.client:
            response = self.client.get('/account', follow_redirects = True)
            self.assertEqual(response.status_code, 200)


    #test the registration of a user is working
    def test_add_user(self):
        with self.client:
            response = self.client.post('/register/personal', data = TEST_USER, follow_redirects = True)
            
            user = User.query.get(2)
            self.assertIn(b"Successfully Registered!", response.data)
            self.assertTrue(user.first == "Samuel")
            self.assertTrue(user.last == "Barnes")
            self.assertFalse(user.email == "bob@man.com")


     #test the registration of a user is working
    def test_add_user_incorrect(self):
        with self.client:
            TEST_USER['email'] = 'barnes'
            response = self.client.post('/register/personal', data = TEST_USER, follow_redirects = True)

            self.assertIn(b'Invalid email address.', response.data)
            self.assertIn('/register/personal', request.url)
            TEST_USER['email'] = 'barnes.samb@gmail.com'



    def test_add_user_email_taken(self):
        TEST_USER['email'] = 'example@user.com'
        response = self.client.post('/register/personal', data = TEST_USER, follow_redirects = True)

        self.assertIn(b'email already taken', response.data)
        TEST_USER['email'] = 'barnes.samb@gmail.com'

        

    def test_login_user(self):
        with self.client:
            response = self.client.post('/login', data = dict(
                email = 'example@user.com', password = 'python', remember = False
            ), follow_redirects = True)

            self.assertIn(b"Successfully Logged In!", response.data)
            self.assertTrue(current_user.id == 1)
            self.assertFalse(current_user.id == 20)


    def test_user_stays_loggedin(self):
        with self.client:
            self.client.post('/login', data = dict(
                email = 'example@user.com', password = 'python', remember = False
            ), follow_redirects = True)

            self.client.get('/', follow_redirects = True)
            self.client.get('/account', follow_redirects = True)
            self.client.get('/', follow_redirects = True)

            self.assertTrue(current_user.first == 'Example')
            self.assertTrue(current_user.last == 'User')
            self.assertFalse(current_user.email == 'bob@test.com')


    def test_login_redirect_if_loggedin(self):
        with self.client:
            self.client.post('/login', data = dict(
                email = 'example@user.com', password = 'python', remember = False
            ), follow_redirects = True)

            self.client.get('/login', follow_redirects = True)
            self.assertIn('/index', request.url) #redirect to home page if logged in
            

    def test_account_needs_login(self):
        with self.client:
            self.client.get('/account', follow_redirects = True)
            self.assertIn('/login', request.url) #redirect to login page


    def test_add_user_listing(self):
        with self.client:
            self.client.post('/login', data = dict(
                email = 'example@user.com', password = 'python', remember = False
            ), follow_redirects = True)

            response = self.client.post('/account/create', data = flatten({
                    'listing': TEST_LISTING,
                    'address': TEST_ADDRESS
                }), follow_redirects = True, content_type='multipart/form-data')

            listing = Listing.query.get(1)
            self.assertIn(b"Listing created successfully", response.data)
            self.assertTrue(listing.name == "Example Listing")
            self.assertTrue(listing.price == 10.05)


    def test_add_company_listing(self):
        with self.client:
            self.client.post('/register/company', data = flatten({
                    'about_you': TEST_USER,
                    'about_company': TEST_COMPANY,
                    'company_image': TEST_COMPANY_IMAGE,
                    'address': TEST_ADDRESS
                }), follow_redirects = True, content_type='multipart/form-data')

            response = self.client.post('/login', data = dict(
                email = 'barnes.samb@gmail.com', password = 'python', remember = False
            ), follow_redirects = True)

            response = self.client.post('/account/create', data = flatten({
                    'listing': TEST_LISTING
                }), follow_redirects = True, content_type='multipart/form-data')

            listing = Listing.query.get(1)
            self.assertIn(b"Listing created successfully", response.data)
            self.assertTrue(listing.name == "Example Listing")
            self.assertTrue(listing.price == 10.05)            

            
    def test_add_company(self):
        with self.client:
            response = self.client.post('/register/company', data = flatten({
                    'about_you': TEST_USER,
                    'about_company': TEST_COMPANY,
                    'company_image': TEST_COMPANY_IMAGE,
                    'address': TEST_ADDRESS
                }), follow_redirects = True, content_type='multipart/form-data')
            
            #check the user was added
            user = User.query.get(2)
            self.assertTrue(user.first == "Samuel")
            self.assertTrue(user.last == "Barnes")
            self.assertFalse(user.email == "bob@man.com")

            #check the address was added
            address = Address.query.get(1)
            self.assertTrue(address.street1 == "Example")
            self.assertTrue(address.street2 == "Street")
            self.assertFalse(address.city == "London")

            #check the company was added
            company = Company.query.get(1)
            self.assertTrue(company.name == "Sams company")
            self.assertTrue(company.typeof == "cafe")
            self.assertTrue(company.user_id == user.id) #check there is a relation between the company and the user
            self.assertTrue(company.address_id == address.id) #check there is a relation between the address and the company


    def test_buy_listing(self):
        with self.client:
            self.client.post('/login', data = dict(
                email = 'example@user.com', password = 'python', remember = False
            ), follow_redirects = True)

            self.client.post('/account/create', data = flatten({
                    'listing': TEST_LISTING,
                    'address': TEST_ADDRESS
                }), follow_redirects = True, content_type='multipart/form-data')

            response = self.client.post('/listing/buy/1', data = dict(), follow_redirects = True)

            order = Orders.query.get(1)
            self.assertIn('account/reciept/1', request.url)
            self.assertTrue(order.user_id == current_user.id)
            self.assertTrue(order.listing_id == 1)

    def test_buy_listing_quantity_decrease(self):
        with self.client:
            self.client.post('/login', data = dict(
                email = 'example@user.com', password = 'python', remember = False
            ), follow_redirects = True)

            TEST_LISTING['quantity'] = '2'

            self.client.post('/account/create', data = flatten({
                    'listing': TEST_LISTING,
                    'address': TEST_ADDRESS
                }), follow_redirects = True, content_type='multipart/form-data')

            listing = Listing.query.get(1)
            self.assertTrue(listing.quantity == 2)
            self.assertTrue(not listing.sold)
            self.assertFalse(listing.sold)

            self.client.post('/listing/buy/1', data = dict(), follow_redirects = True)

            self.assertTrue(listing.quantity == 1)
            self.assertFalse(listing.quantity == 2)
            self.assertTrue(not listing.sold)
            self.assertFalse(listing.sold)

            self.client.post('/listing/buy/1', data = dict(), follow_redirects = True)

            self.assertTrue(Listing.sold)
            self.assertTrue(listing.quantity == 0)
            self.assertFalse(Listing.quantity == 2)


    def test_claim_listing(self):
        with self.client:
            self.client.post('/login', data = dict(
                email = 'example@user.com', password = 'python', remember = False
            ), follow_redirects = True)

            self.client.post('/account/create', data = flatten({
                    'listing': TEST_LISTING,
                    'address': TEST_ADDRESS
                }), follow_redirects = True, content_type='multipart/form-data')

            self.client.post('/listing/buy/1', data = dict(), follow_redirects = True)

            order = Orders.query.get(1)
            self.assertTrue(not order.claimed)
            self.assertFalse(order.claimed)

            response = self.client.post('/claimed/order/1', data = dict(), follow_redirects = True)

            self.assertTrue('/index', request.url)
            self.assertTrue(order.claimed)
            self.assertFalse(not order.claimed)


    def test_update_details(self):
        with self.client:
            self.client.post('/login', data = dict(
                email = 'example@user.com', password = 'python', remember = False
            ), follow_redirects = True)

            self.assertTrue(current_user.email == "example@user.com")

            response = self.client.post('/account/security/update_user', data = dict(
                first = TEST_USER['first'], last = TEST_USER['last'], email = TEST_USER['email']
            ), follow_redirects = True)

            self.assertIn(b"Successfully updated details!", response.data)
            self.assertTrue(current_user.email == TEST_USER['email'])
            self.assertFalse(current_user.email == 'example@user.com')


    def test_update_details_incorrect(self):
        with self.client:
            self.client.post('/login', data = dict(
                email = 'example@user.com', password = 'python', remember = False
            ), follow_redirects = True)

            response = self.client.post('/account/security/update_user', data = dict(
                first = TEST_USER['first'], last = TEST_USER['last'], email = 'example'
            ), follow_redirects = True)

            self.assertIn(b"Invalid email address.", response.data)
            self.assertFalse(current_user.email == "example")
            self.assertTrue(current_user.email == "example@user.com")

    
    def test_update_password(self):
        with self.client:
            self.client.post('/login', data = dict(
                email = 'example@user.com', password = 'python', remember = False
            ), follow_redirects = True)

            self.assertTrue(bcrypt.check_password_hash(current_user.pwd, 'python'))

            response = self.client.post('/account/security/update_password', data = dict(
                oldPassword = 'python', password = 'test', confirmPassword = 'test'
            ), follow_redirects = True)

            self.assertIn(b"Successfully Updated Password!", response.data)
            self.assertTrue(bcrypt.check_password_hash(current_user.pwd, 'test'))
            self.assertFalse(bcrypt.check_password_hash(current_user.pwd, 'python'))


    def test_update_password_incorrect(self):
        with self.client:
            self.client.post('/login', data = dict(
                email = 'example@user.com', password = 'python', remember = False
            ), follow_redirects = True)

            response = self.client.post('/account/security/update_password', data = dict(
                oldPassword = 'test', password = 'test123', confirmPassword = 'test'
            ), follow_redirects = True)


            # self.assertIn(b"Password incorrect", response.data)
            self.assertIn(b"Field must be equal to password.", response.data)
            self.assertTrue(bcrypt.check_password_hash(current_user.pwd, 'python'))
            self.assertFalse(bcrypt.check_password_hash(current_user.pwd, 'test'))


if __name__ == "__main__":
    print("\n------ Running Tests ------\n")
    unittest.main(verbosity=2)