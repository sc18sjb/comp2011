var pageCount = 0;

$('#create-form-1').submit(function(e) {
    var errorIds = ['lname-error', 'quantity-error', 'ldescription-error', 'date-error', 'time-error', 'price-error'];
    $.each(errorIds, function(key, value) {
        $('#' + value).text("");
        $('#' + value).addClass('d-none');
    });

    $.ajax({
        url: '/account/create/response-1',
        type: 'POST',
        data: $('#create-form-1').serialize(),
        success: function(data) {
            if (data.error) {
                $.each(data.error, function(key, value) {
                    $('#' + key + '-error').text(value);
                    $('#' + key + '-error').removeClass('d-none');
                });
            } else {
                pageCount++;
                changePage(pageCount, "next", getPageLength($('#create-form-1'), 2));
                $('#btn-prev').removeClass('d-none');

            }
        },
        error: function(error) {
            alert(error);
        }
    });

    e.preventDefault();
});


$('#create-image-upload').on('change', function(e) {
    var formData = new FormData($('#create-form-2')[0]);
    
    // remove the existing errors
    $('#image-error').text("");
    $('#image-error').addClass('d-none');

    $.ajax({
        url: '/account/create/response-2',
        method: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(data) {
            if (data.error) {
                $.each(data.error, function(key, value) {
                    $('#' + key + '-error').text(value);
                    $('#' + key + '-error').removeClass('d-none');
                });
            } else {
                $('.listing-image').css('background-image', 'url(' + data.image + ')');
                $('#listing_image-image_data').attr('value', data.image);
            }
        },
        error: function(error) {
            alert(error);
        }
    });
});


$('#create-form-2.no-submit').submit(function(e) {
    pageCount++;
    changePage(pageCount, "next", getPageLength($(this), 2));
    e.preventDefault();
});

$('#create-form-2.submit').submit(function(e) {
    e.preventDefault();
    combineForms();
});


$('#create-form-3').submit(function(e){
    var errorIds = ['street1-error', 'street2-error', 'city-error', 'state-error', 'postal-error', 'country-error'];
    $.each(errorIds, function(key, value) {
        $('#' + value).text("");
        $('#' + value).addClass('d-none')
    });

    $.ajax({
        url: '/account/create/response-3',
        method: 'POST',
        data: $('#create-form-3').serialize(),
        success: function(data) {
            if (data.error) {
                $.each(data.error, function(key, value) {
                    $('#' + key + '-error').text(value);
                    $('#' + key + '-error').removeClass('d-none');
                });
            } else {
                // address = `${data.street1}, ${data.street2}, ${data.city}, ${data.state}, ${data.postal}, ${data.country}`;
                address = data.street1 + ", " + data.street2 + ", " + data.city + ", " + data.state + ", " + data.postal + ", " + data.postal;

                // addressExists(address, combineForms);
                geocoder = new google.maps.Geocoder();

                geocoder.geocode({'address': address}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        combineForms();
                    } else {
                        $('#street1-error').text('address not valid');
                        $('#street1-error').removeClass('d-none');
                    }
                });
            }
        },  
        error: function(error) {
            alert(error);
        }
    });
    e.preventDefault();
});


$('#create-form-4').submit(function(e) {
    $('#create-form-3').submit();
    e.preventDefault();
});