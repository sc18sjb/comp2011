from flask_wtf import FlaskForm  
from wtforms import IntegerField, StringField, SubmitField, PasswordField, BooleanField, SelectField, HiddenField, FormField, FloatField, TextAreaField
from wtforms.fields.html5 import DateField, TimeField
from flask_wtf.file import FileField, FileAllowed
from wtforms.validators import DataRequired, Length, Email, EqualTo, Optional, ValidationError, InputRequired, NumberRange
from datetime import datetime
import pycountry


class CountrySelectField(SelectField):
    def __init__(self, *args, **kwargs):
        super(CountrySelectField, self).__init__(*args, **kwargs)
        self.choices = [(country.alpha_2, country.name) for country in pycountry.countries]


class buyListingForm(FlaskForm):
    submit = SubmitField('Buy Now')

class registerAddressForm(FlaskForm):
    street1 = StringField('Address Line 1', validators=[DataRequired(),
                                                        Length(min=1, max=256)],
                                                        render_kw={"placeholder": "Enter Address Line 1"}) 
    street2 = StringField('Address Line 2', validators=[Length(max=256)],
                                                        render_kw={"placeholder": "Enter Address Line 2"})
    city = StringField('City', validators=[DataRequired(),
                                            Length(min=1, max=128)],
                                            render_kw={"placeholder": "Enter City"})
    state = StringField('State / County', validators=[DataRequired(),
                                                        Length(min=1, max=128)],
                                                        render_kw={"placeholder": "Enter State / County"})
    postal = StringField('Postal / Zip Code', validators=[DataRequired(),
                                                            Length(min=1, max=16)],
                                                            render_kw={"placeholder": "Enter Postal / Zip Code"})
    country = CountrySelectField('Country')
    submit = SubmitField('submit')



class listingImageForm(FlaskForm):
    image = FileField('Image (optional)', validators=[FileAllowed(['png', 'jpg', 'jpeg'])])
    image_data = HiddenField('image', default=None)
    submit = SubmitField('submit')


class listingForm(FlaskForm):
    lname = StringField('What are you selling', validators=[DataRequired(),
                                                                Length(min=1, max=256)],
                                                                render_kw={"placeholder": "Enter Item"})
    ldescription = TextAreaField('What would users get', validators=[DataRequired()], render_kw={"placeholder": "Tell users what they might expect to find"})
    
    price = StringField('Price', render_kw={"placeholder": "Free!"})
    
    date = DateField('Collection date', validators=[InputRequired()],
                                                                format='%Y-%m-%d')
    time = TimeField('Collection time', validators=[InputRequired()],
                                                                    format='%H:%M')
    quantity = IntegerField('Quantity selling',  validators=[DataRequired(), 
                                                                        NumberRange(min=1, max=10)],
                                                                        render_kw={"placeholder": "Enter Quantity"})
    # image = FileField('Image (optional)', validators=[FileAllowed(['png', 'jpg', 'jpeg'])])
    submit = SubmitField('List')

    def validate_price(self, price): 
        if price.data.replace(' ', '') != '':
            try:
                price.data = float(price.data)

                if price.data < 0:
                    raise ValidationError("not a valid price")
            except Exception:
                raise ValidationError("Not a valid price")

    def validate_date(self, date):
        if str(date.data) < datetime.today().strftime('%Y-%m-%d'):
            raise ValidationError('Cant set collection date before today!')

    def validate_time(self, time):
        if str(self.date.data) == datetime.today().strftime('%Y-%m-%d'):
            if str(time.data) < datetime.today().strftime('%H:%M'):
                raise ValidationError('Cant set collection today before current time!')



class createListingForm(FlaskForm):
    listing = FormField(listingForm)
    listing_image = FormField(listingImageForm)
    address = FormField(registerAddressForm)
    submit = SubmitField('List')

class unlistForm(FlaskForm):
    submit = SubmitField("Unlist Item")

class relistForm(FlaskForm):
    submit = SubmitField('Relist Item')

