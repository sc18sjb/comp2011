from app import bcrypt
from flask_wtf import FlaskForm  
from wtforms import IntegerField, StringField, SubmitField, PasswordField, BooleanField, SelectField, HiddenField, FormField, FloatField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, EqualTo, Optional, ValidationError, InputRequired, NumberRange
from app.models import User, Address


class loginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(),
                                                Email(),
                                                Length(min=1, max=128)],
                                                render_kw={"placeholder": "Enter Email"})
    password = PasswordField('Password', validators=[DataRequired()], 
                                                render_kw={"placeholder": "Enter Password"})
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')      

    def validate(self):
        self.user = User.query.filter_by(email = self.email.data).first()
        return super(loginForm, self).validate()

    def validate_password(self, password):
        if self.user:
            if self.user.pwd is None or self.user.external_type != 'native':
                raise ValidationError('password is incorrect')
            elif not bcrypt.check_password_hash(self.user.pwd, password.data):
                raise ValidationError('password is incorrect')
        else:
            raise ValidationError('email or password inccorect')